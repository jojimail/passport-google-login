var express = require('express')
var app = express();

var bodyParser = require('body-parser');
var session = require('express-session');

var Sequelize = require('sequelize');
var sequelize = new Sequelize('learn', 'root', 'Sellerworx123');

var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

/**
 * User Model
 */
var User = sequelize.define('user', {
    email: Sequelize.STRING
});

/**
 * Express settings loading
 */
app.use(express.static('public'));
app.use(bodyParser());
app.use(session({
    secret: 'keyboard cat'
}));
app.use(passport.initialize());
app.use(passport.session());

/**
 * Google strategy
 */
var googleStrategyOptions = {
    clientID: '744258755134-8m72ifemq60134hgtbr2kbfp38ssbrss.apps.googleusercontent.com',
    clientSecret: 'cTDdtRtJSWmWEPmz2hr7qZ3k',
    callbackURL: "http://localhost:3000/auth/google/callback"
};
var googleStrategyHandler = function(accessToen, refreshToken, profile, done) {
    var username = profile.id;
    if(profile.emails && profile.emails.length > 0 && profile.emails[0].value)
      username = profile.emails[0].value;

    process.nextTick(function() {
        User
          .findOrCreate({
              where: {
                email: username
              }
          })
          .then(function(user){
              return done(null, user[0]);
          })
          .catch(function(error) {
              return done(error);
          });
    });
};

var isAuthenticatedMiddleware = function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect('/');
};


passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User
        .findOne({
            where:{
                id : id
            }
        })
        .then(function(user) {
            done(null, user);
        })
        .catch(function(error) {
            done(error);
        });
});

passport.use(new GoogleStrategy(googleStrategyOptions, googleStrategyHandler));

/**
 * Routes
 */
app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

app.get('/auth/google',
    passport.authenticate('google', { scope: [
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ] 
    })
);

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/' }),
        function(req, res) {
            res.redirect('/done');
        }
    );

/**
 * Protected routes
 */
app.get('/done', isAuthenticatedMiddleware,
    function(req, res) {
        res.send('LOGGED IN PAGE, user : ' + req.user.email);
    }
);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})